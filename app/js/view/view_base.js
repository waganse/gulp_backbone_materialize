define([
    'jquery',
    'backbone',
    'underscore',
    'model/model_list',
    'template/template_list',
    'const',
    'util',
    'nouislider',
    'toastr',
    'colorpicker'
], function($, Backbone, _, Models, JST, CONST, UTIL, noUiSlider, toastr) {
    var AppView,
        SearchView,
        ItemListView,
        ItemView;

    AppView = Backbone.View.extend({

        el: $('#content-wrapper'),

        events: {
            'click .js-totop': 'onClickTop'
        },

        itemListView: null,

        searchView: null,

        initialize: function() {
            _.bindAll(this, 'render');
            this.render();
        },

        render: function() {
            var self = this;

            // View initialize
            self.initViews();

            // SVG
            self.svg();

            self.tooltip();

            // Side nav
            self.$el.find('.js-menu-trigger').sideNav({
                edge: 'right'
            });

            $(window).on('scroll', function() {
                self.onScroll();
            });
        },

        initViews: function() {},

        svg: function() {
            $.get(SVG_PATH, function(data) {
                $('body').prepend(new XMLSerializer().serializeToString(data.documentElement));
            });
        },

        tooltip: function() {
            this.$el.find('.js-tooltip').tooltipster({
                theme: 'tooltipster-light',
                maxWidth: 300
            });
        },

        onClickTop: function(e) {
            var $self = $(e.currentTarget);

            $self.addClass('animate');
            $('html,body').animate({scrollTop: 0}, 500);
        },

        onScroll: function() {
            var $w = $(window),
                $totop = this.$el.find('.js-totop'),
                h = $w.height() * 0.5,
                scrollTop = $w.scrollTop();

            if (h < scrollTop) {
                $totop.addClass('active');
            } else {
                $totop.removeClass('active');
            }
        }

    });

    SearchView = Backbone.View.extend({
        itemListView: null,

        el: $('#search-box'),

        events: {
            'click .js-search': 'onClickSearch',
            'change #js-partner-select': 'getPartner',
            'change #js-cid-select': 'getApiKey',
            'change #numberOfRooms': 'changeRoomNum',
            'click .btn-count': 'counter'
        },

        initialize: function() {
            _.bindAll(this, 'render');
            this.render();
        },

        render: function() {},

        getPartner: function(e) {
            var self = this,
                partnerId = $(e.currentTarget).val();

            $.ajax({
                url: CONST.API_KEY,
                data: { partner_id: partnerId },
                dataType: 'json'
            })
            .done(function(res) {
                var $cid = self.$el.find('#js-cid-select'),
                    $apikey = self.$el.find('#apiKey'),
                    $sharedSecret = self.$el.find('#sharedSecret');

                $cid.empty();
                $apikey.val('');

                CONFIG.PARTNER = {
                    id: partnerId,
                    keys: res
                }
                _.each(res, function(item) {
                    $cid.append(JST.selectCid(item));
                });
                $apikey.val(res[0].api_key);
                $sharedSecret.val(res[0].shared_secret);

                $cid.material_select();
            })
            .fail(function(e) {
                console.log(res);
            });

            e.stopPropagation();
        },

        getApiKey: function(e) {
            var self = this,
                $target = $(e.currentTarget);

            _.each(CONFIG.PARTNER.keys, function(key) {
                if ($target.val() === key.cid) {
                    $('#apiKey').val(key.api_key);
                    $('#sharedSecret').val(key.shared_secret);
                }
            });
        },

        onClickSearch: function() {
            this.formatParamObj();

            this.createListView();

            this.waveEffect();

            return false;
        },

        formatParamObj: function() {
            var self = this,
                queryArray = $('#js-search-form').serializeArray(),
                commaArray = {};

            $('.js-loader').addClass('active');

            _.each(queryArray, function(item) {
                if (CONST.COMMA_PARAMS.indexOf(item.name) >= 0) {
                    if (!commaArray[item.name]) {
                        commaArray[item.name] = [];
                    }
                    commaArray[item.name].push(item.value);
                }
            });

            _.each(commaArray, function(arr, key) {
                queryArray.push({
                    name: key,
                    value: arr.join(',')
                });
            });

            CONFIG.FETCH_PARAM = self.convertParamObj(queryArray);
            CONFIG.REQUEST_URL = CONST.EAN_HOTELS + UTIL.getQueryStr(CONFIG.FETCH_PARAM);

        },

        convertParamObj: function(sirializedArray) {
            var paramObj = {},
                roomNum = 0,
                roomArr = [];


            _.each(sirializedArray, function(obj) {
                var name = obj.name,
                    value = obj.value;

                if (value === '') { return; }

                paramObj[name] = value;
            });

            return paramObj;
        },

        counter: function() {},

        // setSig: function() {
        //     this.$el.find('#sig').val(this.getSig());
        // },

        // getSig: function() {
        //     var d = new Date(),
        //         unixTime = Math.floor( d.getTime() / 1000 ) + '',
        //         str = CONFIG.API_KEY + CONFIG.SHARED_SECRET + unixTime;

        //     str = md5(str);

        //     return str;
        // },

        waveEffect: function() {
            var self = this;

            setTimeout(function() {
                self.$el.find('.waves-ripple').remove();
            }, 750);
        },

        changeRoomNum: function() {
        },

        close: function() {
            this.unbind();
            this.remove();
        },

        createListView: function() {}
    });

    ItemListView = Backbone.View.extend({
        itemViews: [],

        el: $('#item-list'),

        initialize: function() {
            _.bindAll(this, 'render');
            this.render();
        },

        render: function() {
            var self = this;

            if (CONFIG.CACHE_KEY !== '') {
                CONFIG.CACHE_KEY = '';
                CONFIG.REMOVE_FLAG = false;
            } else {
                CONFIG.REMOVE_FLAG = true;
            }

            self.collection
                .fetch({ remove: CONFIG.REMOVE_FLAG, wait: true, dataType: 'json', data: CONFIG.FETCH_PARAM })
                .done(function(response) {
                    CONFIG.JSON_RESPONSE = JSON.stringify(response, null, 2);

                    $('#url').addClass('active').text(CONFIG.REQUEST_URL);
                    $('#sig').val(CONFIG.QUERY_OBJ.sig);
                    self.add();
                })
                .fail(function(response) {
                    console.log(response);
                    toastr.error('Please check partner information.');
                })
                .always(function() {
                    setTimeout(function() {
                        $('.js-loader').removeClass('active');
                    }, 0);
                });
        },

        add: function() {
            // var self = this,
            //     el = document.getElementById('json-response');

            // self.renderItems();

            // $('#response-container').find('.CodeMirror').remove();
            // $('#json-response').html(CONFIG.JSON_RESPONSE);

            // CodeMirror.fromTextArea(el, {
            //     lineNumbers: true,
            //     mode: 'application/ld+json',
            //     readOnly: true,
            //     foldGutter: true,
            //     gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
            //     theme: 'material',
            //     viewportMargin: Infinity
            // });
        },

        // autoTextArea: function() {
        //     var $input = this.$el.find("#json");

        //     $input.height(30); //init
        //     $input.css("lineHeight", "20px"); //init

        //     $input.on("input", function(evt) {
        //       if (evt.target.scrollHeight > evt.target.offsetHeight) {
        //           $(evt.target).height(evt.target.scrollHeight);
        //       } else {
        //           var lineHeight = Number($(evt.target).css("lineHeight").split("px")[0]);
        //           while (true) {
        //               $(evt.target).height($(evt.target).height() - lineHeight);
        //               if (evt.target.scrollHeight > evt.target.offsetHeight) {
        //                   $(evt.target).height(evt.target.scrollHeight);
        //                   break;
        //               }
        //           }
        //       }
        //     });

        //     $input.trigger('input');
        // },

        reset: function() {
            _.each(this.itemViews, function(view) {
                view.close();
            });
            this.itemViews.length = 0;

            this.collection.remove(this.collection.models);
            // this.$el.empty();
            this.unbind();
            // this.render();
        }

    });

    ItemView = Backbone.View.extend({

        $parentEl: $('#item-list'),

        close: function() {
            this.unbind();
            this.remove();
        }

    });

    return {
        AppView: AppView,
        SearchView: SearchView,
        ItemListView: ItemListView,
        ItemView: ItemView
    };
});