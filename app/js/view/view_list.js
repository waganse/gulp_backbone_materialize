define([
    'jquery',
    'backbone',
    'underscore',
    'view/view_base',
    'model/model_list',
    'template/template_list',
    'const',
    'nouislider',
    'colorpicker'
], function($, Backbone, _, ViewBase, Models, JST, CONST, noUiSlider, colorpicker) {
    var AppView,
        SearchView,
        ItemListView,
        ItemView;

    AppView = ViewBase.AppView.extend({
        initViews: function() {
            // Search
            if (!this.searchView) {
                this.searchView = new SearchView();
            } else {
                this.searchView;
            }
        }
    });

    SearchView = ViewBase.SearchView.extend({
        roomIdx: 1,

        render: function() {
            // Select
            this.$el.find('select').material_select();

            // Color picker
            this.colorPicker();

            // Size
            this.sizeSlider();
        },

        colorPicker: function() {
            var $picker = this.$el.find('#colorpicker');

            $picker.colorpicker({
                color: $picker.css('background-color'),
                format: 'hex'
            }).on('changeColor', function(e) {
                $('.icon').css('color', e.color.toHex());
                $('.sprite-svg').css('fill', e.color.toHex());
            });
        },

        sizeSlider: function() {
            var self = this,
                slider = document.getElementById('starRating');

            if (slider) {
                noUiSlider.create(slider, {
                    start: CONST.FONT_SIZE.start,
                    // connect: true,
                    step: CONST.FONT_SIZE.step,
                    range: CONST.FONT_SIZE.range,
                    format: wNumb({
                        decimals: CONST.FONT_SIZE.decimals
                    })
                });

                slider.noUiSlider.on('update', function(values) {
                    console.log(values);
                    $('.icon').css('font-size', values[0] + 'px');
                $('.sprite-svg').css('width', values[0] + 'px');
                $('.sprite-svg').css('height', values[0] + 'px');
                });

            }
        }

    });

    ItemListView = ViewBase.ItemListView.extend({
        renderItems: function() {
            console.log('renderItems');
        }
    });

    ItemView = ViewBase.ItemView.extend({
        tpl: JST.item
    });

    return {
        AppView: AppView,
        SearchView: SearchView,
        ItemListView: ItemListView,
        ItemView: ItemView
    };
});
