require.config({
    paths: {
        'jquery': '../../bower_components/jquery/dist/jquery',
        'jqueryui': '../../bower_components/jquery-ui/jquery-ui',
        'underscore': '../../bower_components/underscore/underscore',
        'backbone': '../../bower_components/backbone/backbone',
        'util': 'util',
        'const': 'constants',
        'config': 'config',
        'frosty': '../../bower_components/frosty/dist/js/frosty.min',
        'nouislider': '../../bower_components/Materialize/extras/noUiSlider/nouislider',
        'hammerjs': '../../bower_components/Materialize/js/hammer.min',
        'jquery.easing': '../../bower_components/Materialize/js/jquery.easing.1.3',
        'velocity': '../../bower_components/Materialize/js/velocity.min',
        'picker': '../../bower_components/Materialize/js/date_picker/picker',
        'picker.date': '../../bower_components/Materialize/js/date_picker/picker.date',
        'waves': '../../bower_components/Materialize/js/waves',
        'global': '../../bower_components/Materialize/js/global',
        'animation': '../../bower_components/Materialize/js/animation',
        'collapsible': '../../bower_components/Materialize/js/collapsible',
        'dropdown': '../../bower_components/Materialize/js/dropdown',
        'modal': '../../bower_components/Materialize/js/modal',
        'materialbox': '../../bower_components/Materialize/js/materialbox',
        'tabs': '../../bower_components/Materialize/js/tabs',
        'sideNav': 'sideNav',
        'parallax': '../../bower_components/Materialize/js/parallax',
        'scrollspy': '../../bower_components/Materialize/js/scrollspy',
        'tooltip': '../../bower_components/Materialize/js/tooltip',
        'slider': '../../bower_components/Materialize/js/slider',
        'cards': '../../bower_components/Materialize/js/cards',
        'buttons': '../../bower_components/Materialize/js/buttons',
        'pushpin': '../../bower_components/Materialize/js/pushpin',
        'character_counter': '../../bower_components/Materialize/js/character_counter',
        'toasts': '../../bower_components/Materialize/js/toasts',
        'forms': '../../bower_components/Materialize/js/forms',
        'scrollFire': '../../bower_components/Materialize/js/scrollFire',
        'transitions': '../../bower_components/Materialize/js/transitions',
        'jquery.hammer': '../../bower_components/Materialize/js/jquery.hammer',
        'tooltipstar': 'tooltipster',
        'toastr': '../../bower_components/toastr/toastr',
        'colorpicker': 'lib/materialize-colorpicker'
    },

    shim: {
        'underscore': {
            'exports': '_'
        },
        'backbone': {
            'deps': ['jquery', 'underscore'],
            'exports': 'Backbone'
        },
        'frosty': {
            deps: ['jquery']
        },
        'jqueryui': {
            deps: ['jquery']
        },
        'jquery.easing': {
            deps: ['jquery']
        },
        'animation': {
            deps: ['jquery']
        },
        'jquery.hammer': {
            deps: ['jquery', 'hammerjs', 'waves']
        },
        'global': {
            deps: ['jquery']
        },
        'toasts': {
            deps: ['global']
        },
        'collapsible': {
            deps: ['jquery']
        },
        'dropdown': {
            deps: ['jquery']
        },
        'modal': {
            deps: ['jquery']
        },
        'materialbox': {
            deps: ['jquery']
        },
        'parallax': {
            deps: ['jquery']
        },
        'tabs': {
            deps: ['jquery']
        },
        'tooltip': {
            deps: ['jquery']
        },
        'sideNav': {
            deps: ['jquery']
        },
        'scrollspy': {
            deps: ['jquery']
        },
        'forms': {
            deps: ['jquery', 'global']
        },
        'slider': {
            deps: ['jquery']
        },
        'cards': {
            deps: ['jquery']
        },
        'pushpin': {
            deps: ['jquery']
        },
        'buttons': {
            deps: ['jquery']
        },
        'transitions': {
            deps: ['jquery','scrollFire']
        },
        'scrollFire': {
            deps: ['jquery', 'global']
        },
        'waves': {
            exports: 'Waves'
        },
        'character_counter': {
            deps: ['jquery']
        },
        'tooltipster': {
            deps: ['jquery']
        },
        'toastr': {
            deps: ['jquery']
        }
    }
});