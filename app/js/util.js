define([
    'underscore'
], function(_) {

    var UTIL = {

        zeroPad: function(str, size) {
            var padStr = '',
                i;

            size = size || 2;

            for (i = 0; i < size; i ++) {
                padStr += '0';
            }

            padStr += str;

            return padStr.substr(-size);
        },

        getQueryStr: function(paramObj) {
            var str = '?',
                i = 0;

            _.each(paramObj, function(val, key) {
                if (i > 0) {
                    str += '&';
                }
                if (val !== '' && key !== 'sharedSecret') {
                    str += key + '=' + val;
                    i ++;
                }
            });

            return str;
        }

    };

    return UTIL;

});