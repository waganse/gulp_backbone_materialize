define(function() {

    var CONST = {
        EAN_HOTELS: 'https://api.ean.com/ean-services/rs/hotel/v3/list',
        EAN_AVAIL: 'http://api.ean.com/ean-services/rs/hotel/v3/avail',
        EAN_ITIN: 'http://api.ean.com/ean-services/rs/hotel/v3/itin',
        EAN_INFO: 'http://api.ean.com/ean-services/rs/hotel/v3/info',
        EAN_PAYTYPE: 'http://api.ean.com/ean-services/rs/hotel/v3/payment-type',

        API_HOTELS: 'http://localhost/ean_sandbox/hotels/data/',
        API_ITIN: 'http://localhost/ean_sandbox/itin/data/',
        API_AVAIL: 'http://localhost/ean_sandbox/avail/data/',
        API_INFO: 'http://localhost/ean_sandbox/info/data/',
        API_PAYTYPE: 'http://localhost/ean_sandbox/paytype/data/',
        API_PARTNER: 'http://localhost/ean_sandbox/partner/data/',
        API_KEY: 'http://localhost/ean_sandbox/key/data/',
        API_PARENT_REGION: 'http://localhost/ean_sandbox/parentregionlist/data/',
        API_MAPPING: 'http://localhost/ean_sandbox/regioneanhotelidmapping/data/',
        FONT_SIZE: {
            start: 20,
        	range: { min: 20, max: 500 },
        	default: { min: 20, max: 500 },
        	step: 10,
        	decimals: 1
        },
        COMMA_PARAMS: ['propertyCategory', 'options']
    };

    return CONST;

});