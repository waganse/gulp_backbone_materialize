define([
    'backbone',
    'underscore',
    'view/view_list',
    'model/model_list',
    'config'
], function(Backbone, _, Views, Models) {
    var AppRouter;

    AppRouter = Backbone.Router.extend({
        appView: null,

        itemCollection: null,

        routes: {
            '': 'all',
            '!': 'all',
            '!/': 'all'
        },

        initialize: function() {
            this.itemCollection = new Models.ItemCollection();
        },

        all: function() {
            if (this.appView) {
                this.appView.render();
            } else {
                this.appView = new Views.AppView();
            }
        }

    });

    return AppRouter;
});