define([
    'jquery',
    'backbone',
    'underscore',
    'const',
    'util',
], function($, Backbone, _, CONST, UTIL) {
    var ItemCollection,
        ItemModel;

    ItemModel = Backbone.Model.extend();

    ItemCollection = Backbone.Collection.extend({
        query: '',

        model: ItemModel,

        url: function() {
            return CONST.API_HOTELS;
        },

        parse: function(res) {
            var self = this,
                hotelListObj = (res.HotelListResponse)? res.HotelListResponse : null,
                paramObj = res.params;

            CONFIG.QUERY_OBJ = paramObj;
            CONFIG.REQUEST_URL = CONST.EAN_HOTELS + UTIL.getQueryStr(paramObj);

            if (!hotelListObj) {
                return null;
            }

            if (hotelListObj.EanWsError) {
                return hotelListObj;
            }

            CONFIG.CUSTOMER_SESSION_ID = hotelListObj.customerSessionId;
            CONFIG.MORE_RESULTS_AVAILABLE = hotelListObj.moreResultsAvailable;
            CONFIG.CACHE_KEY = hotelListObj.cacheKey;
            CONFIG.CACHE_LOCATION = hotelListObj.cacheLocation;
            CONFIG.CACHE_SUPPLIER_RESPONSE = hotelListObj.cachedSupplierResponse;
            CONFIG.NUMBER_OF_ROOMS_REQUESTED = hotelListObj.numberOfRoomsRequested;
            CONFIG.SIZE = hotelListObj.HotelList['@size'];
            CONFIG.ACTIVE_PROPERTY_COUNT = hotelListObj.HotelList['@activePropertyCount'];


            return hotelListObj.HotelList.HotelSummary;
        },

        getDateTime: function(dateStr) {
            var d = new Date(dateStr),
                year = d.getFullYear(),
                month = d.getMonth() + 1,
                date = d.getDate(),
                hour = d.getHours(),
                min = d.getMinutes(),
                obj = {};

            obj.date = year + '/' + UTIL.zeroPad(month) + '/' + UTIL.zeroPad(date);
            obj.time = UTIL.zeroPad(hour) + ':' + UTIL.zeroPad(min);

            return obj;
        }
    });

    return {
        ItemModel: ItemModel,
        ItemCollection: ItemCollection
    };
});
