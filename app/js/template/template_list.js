define([
    'underscore'
], function(_) {

    var JST = window.JST || {

      selectCid: _.template(
        '<option value="<%= cid %>"><%= cid %> (<%= ratetype.name %>)</option>'
      ),

      item: _.template(
        '<section class="item">\
          <p><%= hotelId %></p>\
          <h2><%= name %></h2>\
          <div class="details">\
            <p class="address"><%= shortDescription %></p>\
          </div>\
        </section>'
      ),

      itemError: _.template(
        '<p><%= EanWsError.category %></p>\
        <p><%= EanWsError.handling %></p>\
        <p><%= EanWsError.presentationMessage %></p>\
        <p><%= EanWsError.verboseMessage %></p>\
        <p><%= customerSessionId %></p>'
      ),

      roomNum: _.template(
        '<div class="row js-room-container">\
          <section class="js-room-info-box">\
            <div class="col l2 m6 s12"></div>\
            <div class="room-info">\
              <div class="col l2 m6 s12">\
                <label>numberOfAdults</label>\
                <div class="count-box js-count-box">\
                  <div class="btn btn-count minus waves-effect waves-light grey darken-2" data-action=false><i class="material-icons icn-info">remove</i></div>\
                  <input type="text" class="js-counter" name="numberOfAdults<%= idx %>" value="1" readonly>\
                  <div class="btn btn-count plus waves-effect waves-light grey darken-2" data-action=true><i class="material-icons icn-info">add</i></div>\
                </div>\
              </div>\
              <div class="col l2 m6 s12">\
                <label>numberOfChildren</label>\
                <div class="count-box js-count-box" data-children=true>\
                  <div class="btn btn-count minus waves-effect waves-light grey darken-2" data-action=false><i class="material-icons icn-info">remove</i></div>\
                  <input type="text" class="js-numchild js-counter" name="numberOfChildren<%= idx %>" value="0" readonly>\
                  <div class="btn btn-count plus waves-effect waves-light grey darken-2" data-action=true><i class="material-icons icn-info">add</i></div>\
                </div>\
              </div>\
            </div>\
          </section>\
        </div>'
      ),

      childAge: _.template(
        '<div class="age-box js-age-box">\
          <div class="col l1 m6 s12">\
            <label>childAge</label>\
            <input type="number" name="childAge<%= idx %>-<%= ageIdx %>" value="0" min="0" max="17">\
          </div>\
        </div>'
      )

    };

    return JST;

});