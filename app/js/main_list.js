require([
    'backbone',
    'underscore',
    'router/router_list',
    'config',
    'frosty',
    'jqueryui',
    'nouislider',
    'jquery.easing',
    'animation',
    'velocity',
    'hammerjs',
    'jquery.hammer',
    'global', // very important do not remove!
    'collapsible',
    'dropdown',
    'modal',
    'materialbox',
    'parallax',
    'tabs',
    'tooltip',
    'waves',
    'toasts',
    'sideNav',
    'scrollspy',
    'forms',
    'slider',
    'cards',
    'pushpin',
    'buttons',
    'scrollFire',
    'transitions',
    'picker',
    'picker.date',
    'character_counter',
    'tooltipster',
    'toastr',
    'colorpicker'
], function(Backbone, _, AppRouter) {
    Backbone.emulateJSON = true;

    new AppRouter();
    Backbone.history.start();

    if (!location.hash) {
        Backbone.history.navigate('!/', {
            replace: true
        });
    }
});