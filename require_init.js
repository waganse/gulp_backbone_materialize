({
    baseUrl: 'app/js/',

    mainConfigFile: 'app/js/require-config.js',

    out: (function() {
        var page = (process.argv.filter(function(arg) {
            return arg.match(/^page=/);
        })[0] || '').replace(/^page=/, '');

        switch (page) {
            case 'list':
                return '.tmp/dist/js/app_list.js';
                break;
            // case 'itin':
            //     return '.tmp/dist/js/app_itin.js';
            //     break;
            // case 'avail':
            //     return '.tmp/dist/js/app_avail.js';
            //     break;
            // case 'info':
            //     return '.tmp/dist/js/app_info.js';
            //     break;
            // case 'paytype':
            //     return '.tmp/dist/js/app_paytype.js';
            //     break;
            // case 'partner':
            //     return '.tmp/dist/js/app_partner.js';
            //     break;
            // case 'key':
            //     return '.tmp/dist/js/app_key.js';
            //     break;
            // case 'ratetype':
            //     return '.tmp/dist/js/app_ratetype.js';
            //     break;
            // case 'region':
            //     return '.tmp/dist/js/app_region.js';
            //     break;
            // case 'deal':
            //     return '.tmp/dist/js/app_deal.js';
            //     break;
        }
    })(),
    // optimize: 'uglify2',
    optimize: '',

    include: (function() {
        var page = (process.argv.filter(function(arg) {
            return arg.match(/^page=/);
        })[0] || '').replace(/^page=/, '');

        switch (page) {
            case 'list':
                return 'main_list';
                break;
            // case 'itin':
            //     return 'main_itin';
            //     break;
            // case 'avail':
            //     return 'main_avail';
            //     break;
            // case 'info':
            //     return 'main_info';
            //     break;
            // case 'paytype':
            //     return 'main_paytype';
            //     break;
            // case 'partner':
            //     return 'main_partner';
            //     break;
            // case 'key':
            //     return 'main_key';
            //     break;
            // case 'ratetype':
            //     return 'main_ratetype';
            //     break;
            // case 'region':
            //     return 'main_region';
            //     break;
            // case 'deal':
            //     return 'main_deal';
            //     break;
        }
    })(),
    name: '../../bower_components/almond/almond'
})
