# Gulp template with materializecss, backbonejs, ect, and stylus

## Installation

### Install Node.js
[Download from Node.js official site](http://nodejs.org/), or use your package management software.

### Install Gulp.js
* `npm install`
  Install gulp related sources

* `bower install`
  Install bower controled sources

## Update materialize bower.json
In order to activate materialize component (noUISlider), update bower.json in /bower_components/Materialize/bower.json.

[Download from here](https://github.com/waganse/bower_materialize)

## Update jquery-ui bower.json
Add jquery-ui.css to main section.

```
{
  "name": "jquery-ui",
  "version": "1.12.1",
  "main": [
    "jquery-ui.js",
    "themes/black-tie/jquery-ui.css"
  ],
  "ignore": [
  ],
  "license": "MIT",
  "dependencies": {
    "jquery": ">=1.6"
  }
}
```

## Update codemirror bower.json

```
{
  "name": "codemirror",
  "main": [
    "lib/codemirror.js",
    "lib/codemirror.css",
    "addon/fold/foldgutter.css",
    "theme/material.css"
  ],
  "ignore": [
    "**/.*",
    "node_modules",
    "components",
    "bin",
    "demo",
    "doc",
    "test",
    "index.html",
    "package.json",
    "mode/*/*test.js",
    "mode/*/*.html"
  ]
}
```

### Install requirejs

```
npm install -g requirejs
```

## Build Tasks
* `gulp serve`
  Start localhost server with BrowserSync.
  Type [Cntl + C] to exit.

* `gulp preview`
  Build preview files.
  Type [Cntl + C] to exit.

* `gulp build`
  Build release files.
  Type [Cntl + C] to exit.
